This is an initial environment setup (with eslint) for a typescript project.
To start programming:
1) Make sure you have the following vscode extentions installed:
    1.1) Install "dbaeumer.vscode-eslint"
    1.2) Install "esbenp.prettier-vscode" (set default formatter to this extention - press "alt" + "shift" + "f" after installation and press the bottom right popup window, then set default to prettier)
2) Edit package.json and change the name, description, main file of your package, and repository info to suit your needs. (main file is set to server.ts by default)
3) Run "npm install" command in console to install all dependencies.
4) Inside vscode:
	4.1) press "ctr" + "p".
	4.2) type ">settings".
	4.3) select "Open Settings (JSON)".
	4.4) make sure the opened file contains the following code:
_____________________________________________________________________________

"eslint.validate":  [
      "javascript",
      "javascriptreact",
      "typescript",
      "typescriptreact",
    ],
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },

_____________________________________________________________________________

	4.5) save and exit.
5) Everything should work now.